package fri.android;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;

public class Slikec extends Activity {
	
	private static final int numImages = 8;	// stevilo slik v zaporedju
	private int selectedImage = 1;
	private PictureView picView = null;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.logW("Created");
        setContentView(R.layout.main);
        
        picView = (PictureView) findViewById(R.id.picture_view);
        
        setAndroidPictures();
//        setSelectedImage(4);
    }
    
    private void setAndroidPictures() {
    	for (int i=0; i<numImages; i++)
    		setImage(Environment.getExternalStorageDirectory().toString()+"/images/android_logo_0"+ i +".png", picView);
    }
    
    private void setAndroidPicture() {
    	Utils.logV("Image %d", selectedImage);
    	setImage(Environment.getExternalStorageDirectory().toString()+"/images/android_logo_0"+ selectedImage +".png", picView);
    }
    
    @SuppressWarnings("unchecked")
	private void setImage(String filename, PictureView view) {
    	Object data = getLastNonConfigurationInstance();
    	ArrayList<Bitmap> images = picView.getImages();
    	if (data!=null) {
    		images = (ArrayList<Bitmap>)data;
    		view.loadImages(images);
            setSelectedImage(selectedImage);
    	}
    	if (images.size() < numImages) {
    		Bitmap image = Utils.loadBitmap(filename);
    		Utils.logV("Loading images.");
        	view.loadImage(image);
    	} else {
            setSelectedImage(selectedImage);
    	}
    }
    
    private void setSelectedImage(int index) {
    	picView.setSelectedImage(true, index);
    	picView.postInvalidate();
    	picView.invalidate();
    }
    
    private void nextImage() {
    	if (selectedImage < 7) {
    		selectedImage++;
	    	setAndroidPicture();
	    	picView.setTransition(true, selectedImage-1);
	
	        Thread nit = new Thread() {
	        	public void run() {
	        		while(picView.transition) {
	        			try {
	        				Thread.sleep(20);
	        				picView.setOffset(-10, 0);
	        				if (picView.image_x < - picView.view_w) {
	        					picView.transition = false;
	        					picView.image_x = 0;
	        				}
	        				picView.postInvalidate();
	        			} catch(Exception e) {};
	        		}
	        	}
	        };
	        nit.start();
    	}
    }
    
    private void previousImage() {
    	if (selectedImage > 0) {
    		selectedImage--;
	    	setAndroidPicture();
	    	picView.setTransition(true, selectedImage+1);
	
	        Thread nit = new Thread() {
	        	public void run() {
	        		while(picView.transition) {
	        			try {
	        				Thread.sleep(20);
	        				picView.setOffset(-10, 0);
	        				if (picView.image_x < - picView.view_w) {
	        					picView.transition = false;
	        					picView.image_x = 0;
	        				}
	        				picView.postInvalidate();
	        			} catch(Exception e) {};
	        		}
	        	}
	        };
	        nit.start();
    	}
    }
    
    private void firstImage() {
    	int a = selectedImage;
    	selectedImage = 0;
    	setAndroidPicture();
    	picView.setTransition(true, a);
    	
        Thread nit = new Thread() {
        	public void run() {
        		while(picView.transition) {
        			try {
        				Thread.sleep(20);
        				picView.setOffset(-10, 0);
        				if (picView.image_x < - picView.view_w) {
        					picView.transition = false;
        					picView.image_x = 0;
        				}
        				picView.postInvalidate();
        			} catch(Exception e) {};
        		}
        	}
        };
        nit.start();
    }
    
    private void lastImage() {
    	int a = selectedImage;
    	selectedImage = numImages-1;
    	setAndroidPicture();
    	picView.setTransition(true, a);
    	
        Thread nit = new Thread() {
        	public void run() {
        		while(picView.transition) {
        			try {
        				Thread.sleep(20);
        				picView.setOffset(-10, 0);
        				if (picView.image_x < - picView.view_w) {
        					picView.transition = false;
        					picView.image_x = 0;
        				}
        				picView.postInvalidate();
        			} catch(Exception e) {};
        		}
        	}
        };
        nit.start();
    }

    @Override
    public void onRestart() {
    	super.onRestart();
    	Utils.logV("onRestart");
    }

    @Override
    public void onStart() {
    	super.onStart();
    	Utils.logV("onStart");
    	
    }

    @Override
    public void onStop() {
    	super.onStop();
    	Utils.logV("onStop");
    }

    @Override
    public void onResume() {
    	super.onResume();
    	Utils.logV("onResume");
    }

    @Override
    public void onPause() {
    	super.onPause();
    	Utils.logV("onPause");
    }

    @Override
    public void onDestroy() {
    	super.onDestroy();
    	Utils.logV("onDestroy");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	PictureView picView = (PictureView) findViewById(R.id.picture_view);
    	picView.saveState(outState);
    	Utils.logV("SavedInstance");
    }
    
    @Override
    public Object onRetainNonConfigurationInstance() {
    	Utils.logV("onRetainNonConfigurationInstance");
    	PictureView picView = (PictureView) findViewById(R.id.picture_view);
    	ArrayList<Bitmap> bmp = picView.getImages();
    	Utils.logV("onRetainNonConfigurationInstance");
        return bmp;
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        Utils.logV("onCreateOptionsMenu");
        return true;
    }
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Utils.logV("onOptionsItemSelected");
		switch (item.getItemId()) {
		case R.id.menu_backward: {
			previousImage();
			return true;
		}
		case R.id.menu_forward: {
			nextImage();
			return true;
		}
		case R.id.menu_first: {
			firstImage();
			return true;
		}
		case R.id.menu_last: {
			lastImage();
			return true;
		}
		}
		return false;
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		Utils.logV("onKeyUp");
		switch (keyCode) {
		case KeyEvent.KEYCODE_DPAD_LEFT:
			previousImage();
			return true;
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			nextImage();
			return true;
		case KeyEvent.KEYCODE_DPAD_UP:
			firstImage();
			return true;
		case KeyEvent.KEYCODE_DPAD_DOWN:
			lastImage();
			return true;
		}
		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		Utils.logV("onTouchEvent");
		if (event.getAction() == MotionEvent.ACTION_DOWN)
			setSelectedImage(0);
		return true;
	}
	
	
}