package fri.android;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Matrix;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;

public class PictureView extends View {
	
//	Bitmap image = null;
	private ArrayList<Bitmap> images = new ArrayList<Bitmap>();
	
	private boolean selected = false;
	private int selectedImage = 0;
	
	int view_w, view_h;
	int image_x, image_y;
	
	boolean transition = false;
	int transitionStart;

	public PictureView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
	}
	
	public void init(Bundle savedInstanceState) {
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus(); //for keyevent to go to this view
		if (savedInstanceState==null) return;
	}
	
	public void saveState(Bundle outState) {
	}
	
	public void loadImage(Bitmap bmp) {
		if (bmp != null) {
			images.add(bmp);
		}
	}
	
	public void loadImages(ArrayList<Bitmap> imgs) {
		if (imgs != null) {
			images = imgs;
		}
	}
	
	public ArrayList<Bitmap> getImages() {
		return images;
	}
	
	public void setSelectedImage(boolean sel, int ind) {
		selected = true;
		selectedImage = ind;
	}
	
	@Override
    protected void onDraw(Canvas canvas) {
		
        super.onDraw(canvas);

		long t1=System.nanoTime();        
        
		if (!selected) {
	    	for (Bitmap image : images) {
	            if (image!=null) {
	        		int x = (int)(Math.random()*(view_w-50));
	        		int y = (int)(Math.random()*(view_h-50));
	//        		int fi = (int)(Math.random()*360);
	        		int fi = 0;
	
	        		Matrix matrika = new Matrix();
	        		matrika.postRotate(fi);
	        	
	       			Bitmap slika = Bitmap.createBitmap(image, 0, 0, 100, 100, matrika, true);
					Rect windowRect = new Rect(x, y, x+50, y+50);
					Rect pictureRect = new Rect(0, 0, slika.getWidth(), slika.getHeight());
		        	canvas.drawBitmap(slika, pictureRect, windowRect, null);
	        	}       	
	        }
		} else {
			if (!transition) {
				Bitmap image = images.get(selectedImage);
	        	if (view_h > view_w) {
					Rect windowRect = new Rect(0, (view_h - (int)(view_w/((double)image.getWidth()/image.getHeight())))/2, view_w, (int)(view_w/((double)image.getWidth()/image.getHeight()))+(view_h - (int)(view_w/((double)image.getWidth()/image.getHeight())))/2);
					Rect pictureRect = new Rect(0, 0, image.getWidth(), image.getHeight());
		        	canvas.drawBitmap(image, pictureRect, windowRect, null);
	        	} else {
					Rect windowRect = new Rect((view_w - (int)(((double)image.getWidth()/image.getHeight())*view_h))/2, 0, (int)(((double)image.getWidth()/image.getHeight())*view_h)+(view_w - (int)(((double)image.getWidth()/image.getHeight())*view_h))/2, view_h);
					Rect pictureRect = new Rect(0, 0, image.getWidth(), image.getHeight());
		        	canvas.drawBitmap(image, pictureRect, windowRect, null);
	        	}
			} else {
					Bitmap image1 = images.get(transitionStart);
					Bitmap image2 = images.get(selectedImage);
		        	if (view_h > view_w) {
						Rect windowRect = new Rect(0+image_x, (view_h - (int)(view_w/((double)image1.getWidth()/image1.getHeight())))/2, view_w+image_x, (int)(view_w/((double)image1.getWidth()/image1.getHeight()))+(view_h - (int)(view_w/((double)image1.getWidth()/image1.getHeight())))/2);
						Rect pictureRect = new Rect(0, 0, image1.getWidth(), image1.getHeight());
			        	canvas.drawBitmap(image1, pictureRect, windowRect, null);
			        	
						windowRect = new Rect(view_w+image_x, (view_h - (int)(view_w/((double)image2.getWidth()/image2.getHeight())))/2, 2*view_w+image_x, (int)(view_w/((double)image2.getWidth()/image2.getHeight()))+(view_h - (int)(view_w/((double)image2.getWidth()/image2.getHeight())))/2);
						pictureRect = new Rect(0, 0, image2.getWidth(), image2.getHeight());
			        	canvas.drawBitmap(image2, pictureRect, windowRect, null);
		        	} else {
						Rect windowRect = new Rect(0, image_y%view_h, (int)(((double)image1.getWidth()/image1.getHeight())*view_h), view_h+image_y);
						Rect pictureRect = new Rect(0, 0, image1.getWidth(), image1.getHeight());
			        	canvas.drawBitmap(image1, pictureRect, windowRect, null);
			        	
						windowRect = new Rect(0, (image_y%view_h), (int)(((double)image2.getWidth()/image2.getHeight())*view_h), view_h+image_y);
						pictureRect = new Rect(0, 0, image2.getWidth(), image2.getHeight());
			        	canvas.drawBitmap(image2, pictureRect, windowRect, null);
		        	}
			}
    		
        	Indicator ind = new Indicator(8, selectedImage, 200, view_w/2, view_h - 20);
        	ind.drawIndicator(canvas);

        	long t2=System.nanoTime();
    		double time=(1.0*t2-t1)/(1000*1000);
    		Utils.logI("drawing "+time); 
		}
    }
	
	public void setTransition(boolean t, int zacetna) {
		transition = t;
		transitionStart = zacetna;
	}
	
	public boolean setOffset(int dx, int dy) {
		image_x = (image_x < view_w) ? image_x + dx : -view_w;
		image_y = (image_y < view_h) ? image_y + dy : -view_h;
		return true;
	}
    
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        view_w=w;
        view_h=h;
        //Log.i(TAG,"New view size "+w+"x"+h);
    }
}
