package fri.android;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;

public class Slikec extends Activity {
	
	private static final int numImages = 8;	// stevilo slik v zaporedju
	private int selectedImage = 1;
	private PictureView picView = null;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.logW("Created");
        setContentView(R.layout.main);
        
        picView = (PictureView) findViewById(R.id.picture_view);
        
        setAndroidPictures();
        setSelectedImage(1);
        
    }
    
    private void setAndroidPictures() {
    	for (int i=0; i<numImages; i++) {
    		File f = new File(Environment.getExternalStorageDirectory(), "android_logo_0" + i + ".png");
    		setImage(f.toString(), picView);
    	}
    }
    
    private void setAndroidPicture() {
		File f = new File(Environment.getExternalStorageDirectory(), "android_logo_0" + selectedImage + ".png");
		setImage(f.toString(), picView);
    }
    
    @SuppressWarnings("unchecked")
	private void setImage(String filename, PictureView view) {
    	Object data = getLastNonConfigurationInstance();
    	ArrayList<Bitmap> images=null;
    	if (data!=null) {
    		images = (ArrayList<Bitmap>)data;
    		view.loadImages(images);
            setSelectedImage(selectedImage);
    	}
    	if (images==null) {
    		Bitmap image = Utils.loadBitmap(filename);
    		Utils.logV("Loading images.");
        	view.loadImage(image);
    	}
    }
    
    private void setSelectedImage(int index) {
    	picView.setSelectedImage(true, index);
    	picView.postInvalidate();
    }
    
    private void nextImage() {
    	if (selectedImage < 7)
    		selectedImage++;
    	setAndroidPicture();
    	picView.postInvalidate();
    }
    
    private void previousImage() {
    	if (selectedImage > 0)
    		selectedImage--;
    	setAndroidPicture();
    	picView.postInvalidate();
    }
    
    private void firstImage() {
    	selectedImage = 0;
    	setAndroidPicture();
    	picView.postInvalidate();
    }
    
    private void lastImage() {
    	selectedImage = numImages-1;
    	setAndroidPicture();
    	picView.postInvalidate();
    }

    @Override
    public void onRestart() {
    	super.onRestart();
    	Utils.logV("onRestart");
    }

    @Override
    public void onStart() {
    	super.onStart();
    	Utils.logV("onStart");
    	
    }

    @Override
    public void onStop() {
    	super.onStop();
    	Utils.logV("onStop");
    }

    @Override
    public void onResume() {
    	super.onResume();
    	Utils.logV("onResume");
    }

    @Override
    public void onPause() {
    	super.onPause();
    	Utils.logV("onPause");
    }

    @Override
    public void onDestroy() {
    	super.onDestroy();
    	Utils.logV("onDestroy");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	PictureView picView = (PictureView) findViewById(R.id.picture_view);
    	picView.saveState(outState);
    	Utils.logV("SavedInstance");
    }
    
    @Override
    public Object onRetainNonConfigurationInstance() {
    	Utils.logV("onRetainNonConfigurationInstance");
    	PictureView picView = (PictureView) findViewById(R.id.picture_view);
    	ArrayList<Bitmap> bmp = picView.getImages();
    	Utils.logV("onRetainNonConfigurationInstance");
        return bmp;
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        Utils.logV("onCreateOptionsMenu");
        return true;
    }
    
}