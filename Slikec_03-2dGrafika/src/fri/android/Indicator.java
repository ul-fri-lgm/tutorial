package fri.android;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class Indicator {
	private int num = 1;
	private int selected = 0;
	
	private int width = 100;
	private int x = 0, y =0;
	
	public Indicator(int num, int selected, int width, int x, int y) {
		this.num = num;
		this.width = width;
		this.x = x;
		this.y  = y;
		this.selected = selected;
	}
	
	public void drawIndicator(Canvas c) {
		Paint p1 = new Paint(Paint.ANTI_ALIAS_FLAG);
		p1.setColor(Color.RED);
		Paint p2 = new Paint(Paint.ANTI_ALIAS_FLAG);
		p2.setColor(Color.BLUE);
		
//		c.drawRect(new Rect((int)(x - width/2) - 10 + (width/(num-1)), y - 10, (int)(x - width/2) + 10 + (width/(num-1)), y + 10), p1);
				
		for (int i = 0; i < num; i++) {
			Utils.logD("pol: "+((x - width/2) - 10 + i*(width/(num-1))));
			if (i == selected) {
				c.drawRect(new Rect((int)(x - width/2) - 10 + i*(width/(num-1)), y - 10, (int)(x - width/2) + 10 + i*(width/(num-1)), y + 10), p1);
				c.drawRect(new Rect((int)(x - width/2) - 5 + i*(width/(num-1)), y - 5, (int)(x - width/2) + 5 + i*(width/(num-1)), y + 5), p2);
			} else {
				c.drawRect(new Rect((int)(x - width/2) - 5 + i*(width/(num-1)), y - 5, (int)(x - width/2) + 5 + i*(width/(num-1)), y + 5), p2);
			}
		}
	}
}
