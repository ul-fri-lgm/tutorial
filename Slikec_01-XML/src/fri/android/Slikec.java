package fri.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;

public class Slikec extends Activity {
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.logW("Created");
        setContentView(R.layout.main);
    } 
    
    public void onRestart() {
    	super.onRestart();
    	Utils.logV("onRestart");
    }
    
    public void onStart() {
    	super.onStart();
    	Utils.logV("onStart");
    	
    }
    
    public void onStop() {
    	super.onStop();
    	Utils.logV("onStop");
    }
    
    public void onResume() {
    	super.onResume();
    	Utils.logV("onResume");
    }
    
    public void onPause() {
    	super.onPause();
    	Utils.logV("onPause");
    }
    
    public void onDestroy() {
    	super.onDestroy();
    	Utils.logV("onDestroy");
    }
    
    public void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	Utils.logV("SavedInstance");
    	
    }
    
    @Override
    public Object onRetainNonConfigurationInstance() {
    	Utils.logV("onRetainNonConfigurationInstance");
        return null;
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }
}