package fri.android;

import android.util.Log;

public class Utils {
	
	public static final boolean DEBUG = true;
	private static final String TAG = "Slikec";
	
    public static void logW(String format, Object...args) {
        Log.w(TAG, String.format(format, args));
    }
	
    public static void logV(String format, Object...args) {
        Log.v(TAG, String.format(format, args));
    }
	
    public static void logI(String format, Object...args) {
        Log.i(TAG, String.format(format, args));
    }
	
    public static void logD(String format, Object...args) {
        Log.d(TAG, String.format(format, args));
    }
    
}