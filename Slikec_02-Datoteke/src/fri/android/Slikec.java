package fri.android;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;

public class Slikec extends Activity {
	
	private static final int numImages = 8;	// stevilo slik v zaporedju
	private int selectedImage = 1;
	private PictureView picView = null;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.logW("Created");
        setContentView(R.layout.main);
        
        picView = (PictureView) findViewById(R.id.picture_view);
        
        setAndroidPicture();
    }
    
    private void setAndroidPicture() {
    	setImage(Environment.getExternalStorageDirectory().toString()+"/images/android_logo_0"+ selectedImage +".png", picView);
    }
    
    private void setImage(String filename, PictureView view) {
    	Object data = getLastNonConfigurationInstance();
    	Bitmap image=null;
    	if (data!=null) {
    		image=(Bitmap) data;
    		if (image.isRecycled()) image=null; //test if image has been recycled
    	}
    	if (image==null) {
    		image = Utils.loadBitmap(filename);
    		Utils.logV("Loading image.");
    	}
    	view.loadImage(image);
    }
    
    private void nextImage() {
    	if (selectedImage < 8)
    		selectedImage++;
    	picView.postInvalidate();
    	setAndroidPicture();
    }
    
    private void previousImage() {
    	if (selectedImage > 0)
    		selectedImage--;
    	picView.postInvalidate();
    	setAndroidPicture();
    }
    
    private void firstImage() {
    	selectedImage = 0;
    	picView.postInvalidate();
    	setAndroidPicture();
    }
    
    private void lastImage() {
    	selectedImage = numImages;
    	picView.postInvalidate();
    	setAndroidPicture();
    }

    @Override
    public void onRestart() {
    	super.onRestart();
    	Utils.logV("onRestart");
    }

    @Override
    public void onStart() {
    	super.onStart();
    	Utils.logV("onStart");
    	
    }

    @Override
    public void onStop() {
    	super.onStop();
    	Utils.logV("onStop");
    }

    @Override
    public void onResume() {
    	super.onResume();
    	Utils.logV("onResume");
    }

    @Override
    public void onPause() {
    	super.onPause();
    	Utils.logV("onPause");
    }

    @Override
    public void onDestroy() {
    	super.onDestroy();
    	Utils.logV("onDestroy");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	PictureView picView = (PictureView) findViewById(R.id.picture_view);
    	picView.saveState(outState);
    	Utils.logV("SavedInstance");
    }
    
    @Override
    public Object onRetainNonConfigurationInstance() {
    	Utils.logV("onRetainNonConfigurationInstance");
    	PictureView picView = (PictureView) findViewById(R.id.picture_view);
    	Bitmap bmp = picView.getImage();
    	Utils.logV("onRetainNonConfigurationInstance");
        return bmp;
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        Utils.logV("onCreateOptionsMenu");
        return true;
    }
    
}