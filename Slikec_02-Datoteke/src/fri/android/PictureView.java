package fri.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;

public class PictureView extends View {
	
	Bitmap image = null;
	int view_w, view_h;

	public PictureView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
	}
	
	public void init(Bundle savedInstanceState) {
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus(); //for keyevent to go to this view
		if (savedInstanceState==null) return;
	}
	
	public void saveState(Bundle outState) {
	}
	
	public void loadImage(Bitmap bmp) {
		if (bmp != null) {
			image = bmp;
		}
	}
	
	public Bitmap getImage() {
		return image;
	}
		
	@Override
    protected void onDraw(Canvas canvas) {
		
        super.onDraw(canvas);

		long t1=System.nanoTime();        
        
        if (image!=null) {
        	if (view_h > view_w) {
				Rect windowRect = new Rect(0, 0, view_w, (int)(view_w/((double)image.getWidth()/image.getHeight())));
//				Utils.logD(""+(image.getWidth()/image.getHeight()));
				Rect pictureRect = new Rect(0, 0, image.getWidth(), image.getHeight());
//				Utils.logD(pictureRect.toString());
	        	canvas.drawBitmap(image, pictureRect, windowRect, null);
	//        	canvas.drawBitmap(image, 0, 0, null);
        	} else {
				Rect windowRect = new Rect(0, 0, (int)(((double)image.getWidth()/image.getHeight())*view_h), view_h);
				Rect pictureRect = new Rect(0, 0, image.getWidth(), image.getHeight());
	        	canvas.drawBitmap(image, pictureRect, windowRect, null);
	//        	canvas.drawBitmap(image, 0, 0, null);
        	}
    		
        	long t2=System.nanoTime();
    		double time=(1.0*t2-t1)/(1000*1000);
    		Utils.logI("drawing "+time);        	
        }
    }
    
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        view_w=w;
        view_h=h;
        //Log.i(TAG,"New view size "+w+"x"+h);
    }
}
